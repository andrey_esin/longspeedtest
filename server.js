var http = require('http'), fs = require('fs');
var port = 8001;

function getDateTime() {
    var date = new Date();
    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;
    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;
    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;
    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;
    return year + "/" + month + "/" + day + " " + hour + ":" + min + ":" + sec;
}

function getLogEntry() {
  return '[' + getDateTime() + '] ';
}

http.createServer(function (request, response) {
  var filename = '/dev/zero';
  response.writeHead(200, {'Content-Type': 'text/plain', 'Content-Length': '999999999999999999999999'});
  console.log(getLogEntry() + 'Hey! Hey! Hey! New connection from ' + request.connection.remoteAddress + '');
  var stream = fs.createReadStream(filename, { bufferSize: 0.5 * 64 * 1024 });
  stream.pipe(response);
}).listen(port);

console.log(getLogEntry() + 'Ok, let\'s go. Listening on ' + port);
